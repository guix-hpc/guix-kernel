                                                              -*- org -*-
#+TITLE: Guix-Jupyter news – history of user-visible changes
#+STARTUP: content hidestars

Copyright © 2021 Ludovic Courtès <ludovic.courtes@inria.fr>

  Copying and distribution of this file, with or without modification,
  are permitted in any medium without royalty provided the copyright
  notice and this notice are preserved.

Please send bug reports to
<https://gitlab.inria.fr/guix-hpc/guix-kernel>.


* Changes in 0.2.2 (since 0.2.1)
** Bug fixes

*** Publish a message upon startup (<https://issues.guix.gnu.org/49355>)

    Previously, Guix-Jupyter would fail to say “hi” on the IOPub socket, which
    newer Notebook versions would interpret as kernel failure.

*** Correctly report idle/busy status
    (<https://gitlab.inria.fr/guix-hpc/guix-kernel/-/issues/3>)

    Until now, Guix-Jupyter would always appear to be busy.  Consequently, the
    favicon and the indicator in the top-right corner of the Jupyter Notebook
    web interface would suggest that.  Additionally, completion requests would
    not be sent, on the grounds that the kernel is busy.

*** Kernel no longer crashes on ‘;;guix download’ directives for local files
    (<https://gitlab.inria.fr/guix-hpc/guix-kernel/-/issues/4>)

    Kernel no longer crashes when passed a ‘download’ directive for a local
    file, as in:

    #+begin_example
      ;;guix download /home/charlie/data.txt 17f5172af292021343644368b5123bf333a8cc8b25818788d9042e2d8dd81293
    #+end_example

* Changes in 0.2.1 (since 0.2.0)

** Bug fix

   Previously Guix-Jupyter would store container data in
   /tmp/guix-kernel, which prevented multiple users from running it on
   the same machine.  It now uses mkdtemp(3) to create that directory.

* Changes in 0.2.0 (since 0.1.0)

** New functionality

*** New ‘;;guix describe’ magic command

    Like the ‘guix describe’ shell command, it returns the currently-used
    channels.

*** New ‘;;guix search’ magic command

    Return the list of packages matching the given patterns.

*** ‘;;guix pin’ and ‘;;guix describe’ output includes links to VCS history

    This allows you to see the version-control system (VCS) history
    leading to the channel commit you’re using.

*** Report build and download progress in the notebook

    When Guix starts downloading or building things, for instance in
    response to an environment creation via ‘;;guix environment’, a
    transient line in the notebook displays what’s being built or
    downloaded.

*** Guile kernel can display SVG pictures

    The built-in Guile kernel now returns SVG for images produced by
    ‘guile-picture-language’ (see
    https://git.elephly.net/software/guile-picture-language.git).

** Bug fixes

*** Gracefully handle package-not-found errors in ‘;;guix environment’

*** Properly complete ‘;;guix’ magic commands

*** Fix potential hang in ‘;;guix pin’

*** Gracefully handle ‘;;guix download’ syntax errors

** Maintenance

*** Support builds with Guile 3.0 and Guile-JSON 4.5
