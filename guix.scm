;;; Run the following command to enter a development environment for
;;; Guix-Jupyter:
;;;
;;;    guix shell --pure

(use-modules ((guix licenses) #:prefix license:)
             (guix packages)
             (guix download)
             (guix git-download)
             (guix gexp)
             (guix utils)
             (guix build-system gnu)
             (gnu packages)
             (gnu packages autotools)
             (gnu packages gnupg)
             (gnu packages guile)
             (gnu packages guile-xyz)
             (gnu packages lisp)
             (gnu packages tls)
             (gnu packages package-management)
             (gnu packages pkg-config))

(package
  (name "guix-kernel")
  (version "0.0.1")
  (source (local-file "." "guix-kernel"
                      #:select? (git-predicate
                                 (dirname (assoc-ref
                                           (current-source-location)
                                           'filename)))
                      #:recursive? #t))
  (build-system gnu-build-system)
  (arguments
   `(#:modules ((srfi srfi-1)
                (ice-9 match)
                (ice-9 popen)
                (ice-9 rdelim)
                ,@%gnu-build-system-modules)
     #:phases
     (modify-phases %standard-phases
       (add-before 'configure 'env
         (lambda _
           (setenv "GUILE_AUTO_COMPILE" "0")))
       (add-after 'install 'sed-kernel-json
         (lambda* (#:key inputs outputs #:allow-other-keys)
           (define (sub-directory suffix)
             ;; Return a procedure that checks whether the given input has
             ;; SUFFIX and, if so, return that sub-directory.
             (match-lambda
               ((label . directory)
                (let ((sub (string-append directory suffix)))
                  (and (file-exists? sub) sub)))))

           (let* ((out   (assoc-ref outputs "out"))
                  (effective
                   (read-line
                    (open-pipe* OPEN_READ "guile" "-c"
                                "(display (effective-version))")))
                  (path (filter-map (sub-directory
                                     (string-append "/share/guile/site/"
                                                    effective))
                                    inputs))
                  (gopath (filter-map (sub-directory
                                       (string-append "/lib/guile/" effective
                                                      "/site-ccache"))
                                      inputs))
                  (kernel-dir (string-append out "/share/jupyter/kernels/guix/")))
             (substitute* (string-append kernel-dir "kernel.json")
               (("-s")
                (string-join
                 (list (string-join path "\",\n\t\t\"")
                       (string-join gopath "\",\n\t\t\"")
                       "-s")
                 "\",\n\t\t\""))
               (("guix-jupyter-kernel.scm")
                (string-append out "/share/guile/site/" effective
                               "/guix-jupyter-kernel.scm")))))))))
  (native-inputs
   (list autoconf
         automake
         pkg-config
         (specification->package "jupyter")
         (specification->package "python-ipython")
         (specification->package "python-ipykernel")))
  (inputs
   (list guix
         (lookup-package-input guix "guile")))
  (propagated-inputs
   (list guile-json-4
         guile-simple-zmq
         guile-gcrypt))

  (home-page "https://gitlab.inria.fr/guix-hpc/guix-kernel")
  (synopsis "Guix kernel for Jupyter")
  (description
   "Guix-Jupyter is a Jupyter kernel.  It allows you to annotate notebooks
with information about their software dependencies, such that code is executed
in the right software environment.  Guix-Jupyter spawns the actual kernels
such as @code{python-ipykernel} on behalf of the notebook user and runs them
in an isolated environment, in separate namespaces.")
  (license license:gpl3+))
