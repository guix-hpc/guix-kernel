;;; Guix-kernel -- Guix kernel for Jupyter
;;; Copyright (C) 2019 Ludovic Courtès <ludovic.courtes@inria.fr>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (guix jupyter containers)
  #:use-module (guix gexp)
  #:use-module (guix store)
  #:use-module (guix monads)
  #:use-module (guix derivations)
  #:use-module (gnu system file-systems)
  #:use-module (gnu system linux-container)
  #:use-module (gnu build linux-container)
  #:use-module (gnu build accounts)
  #:use-module ((guix build utils) #:select (mkdir-p))
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)
  #:export (eval/container*))

(define (bind-mount item)
  (file-system
    (mount-point item)
    (device item)
    (type "none")
    (flags '(bind-mount read-only))
    (check? #f)))

;; Variant of 'eval/container' in (gnu system linux-container).
(define* (eval/container* exp root
                          #:key
                          (guest-uid 1000) (guest-gid 1000)
                          (mounts '())
                          (namespaces %namespaces))
  "Evaluate EXP, a gexp, in a new process executing in separate namespaces as
listed in NAMESPACES, using ROOT as its root directory.  Add MOUNTS, a list
of <file-system>, to the set of directories to mount in the process's mount
namespace.  Return the process' PID."
  (mlet %store-monad ((lowered (lower-gexp exp)))
    (define inputs
      (cons (lowered-gexp-guile lowered)
            (lowered-gexp-inputs lowered)))

    (define items
      (append (append-map derivation-input-output-paths inputs)
              (lowered-gexp-sources lowered)))

    (define (run-guile)
      ;; Run Guile to evaluate EXP.

      ;; There's code out there such as (guix profiles) that looks up
      ;; /etc/passwd right from the top level.  Thus, create it upfront.
      (let ((users  (list (password-entry
                           (name "jupyter")
                           (real-name "Jupyter User")
                           (uid guest-uid) (gid guest-gid)
                           (directory "/home/jupyter"))))
            (groups (list (group-entry (name "users")
                                       (gid guest-gid))
                          (group-entry (gid 65534) ;the overflow GID
                                       (name "overflow")))))
        (mkdir-p "/etc")
        (write-passwd users)
        (write-group groups))

      (apply execl (string-append (derivation-input-output-path
                                   (lowered-gexp-guile lowered))
                                  "/bin/guile")
             "guile"
             (append (append-map (lambda (directory)
                                   `("-L" ,directory))
                                 (lowered-gexp-load-path lowered))
                     (append-map (lambda (directory)
                                   `("-C" ,directory))
                                 (lowered-gexp-load-compiled-path
                                  lowered))
                     (list "-c"
                           (object->string
                            (lowered-gexp-sexp lowered))))))

    (mbegin %store-monad
      (built-derivations inputs)
      (mlet* %store-monad ((closure ((store-lift requisites) items))
                           (mounts -> (append (map bind-mount closure)
                                              mounts)))
        (return (run-container root mounts namespaces 1
                               run-guile
                               #:guest-uid guest-uid
                               #:guest-gid guest-gid))))))
