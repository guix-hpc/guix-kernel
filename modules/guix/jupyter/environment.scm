;;; Guix-kernel -- Guix kernel for Jupyter
;;; Copyright (C) 2019, 2020, 2021 Ludovic Courtès <ludovic.courtes@inria.fr>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (guix jupyter environment)
  #:use-module ((guix ui) #:select (package-specification->name+version+output))
  #:use-module (guix gexp)
  #:use-module (guix channels)
  #:use-module (guix inferior)
  #:use-module (guix profiles)
  #:use-module ((guix utils) #:select (string-replace-substring))
  #:use-module (srfi srfi-26)
  #:use-module (srfi srfi-34)
  #:use-module (srfi srfi-35)
  #:use-module (srfi srfi-71)
  #:use-module (ice-9 match)
  #:use-module (web uri)
  #:export (open-default-inferior
            specifications->manifest

            environment-error?
            package-not-found-error?
            package-not-found-error-name
            package-not-found-error-version
            output-not-found-error?
            output-not-found-error-output
            output-not-found-error-package

            channel-commit-hyperlink))

(define %user-profile
  (string-append %profile-directory "/current-guix"))

(define (open-default-inferior)
  (if (file-exists? %user-profile)
      (open-inferior %user-profile)
      (inferior-for-channels %default-channels)))

(define-condition-type &environment-error &error
  environment-error?)

(define-condition-type &package-not-found-error &environment-error
  package-not-found-error?
  (name     package-not-found-error-name)         ;string
  (version  package-not-found-error-version))     ;string | #f

(define-condition-type &output-not-found-error &environment-error
  output-not-found-error?
  (package  output-not-found-error-package)       ;<inferior-package>
  (output   output-not-found-error-output))       ;string


(define (specification->manifest-entry inferior spec)
  "Lookup package SPEC in INFERIOR.  On success, return a <manifest-entry>
for it; on failure, raise an error."
  (define-values (name version output)
    (package-specification->name+version+output spec))

  (match (lookup-inferior-packages inferior name version)
    (()
     (raise (condition (&package-not-found-error
                        (name name)
                        (version version)))))
    (((? inferior-package? package) _ ...)
     ;; FIXME: Check whether OUTPUT is valid.
     (inferior-package->manifest-entry package output))))

(define (specifications->manifest inferior specs)
  "Resolve all the package SPECS in INFERIOR and return a <manifest>
containing these.  Raise an error such as '&package-not-found-error' if one
of the SPECS could not be resolved."
  (manifest (map (cut specification->manifest-entry inferior <>)
                 specs)))


(define %vcs-web-views
  ;; Hard-coded list of host names and corresponding web view URL templates.
  (let ((labhub-url (lambda (repository-url commit)
                      (string-append
                       (if (string-suffix? ".git" repository-url)
                           (string-drop-right repository-url 4)
                           repository-url)
                       "/commit/" commit))))
    `(("git.savannah.gnu.org"
       ,(lambda (repository-url commit)
          (string-append (string-replace-substring repository-url
                                                   "/git/" "/cgit/")
                         "/log/?id=" commit)))
      ("notabug.org" ,labhub-url)
      ("framagit.org" ,labhub-url)
      ("gitlab.com" ,labhub-url)
      ("gitlab.inria.fr" ,labhub-url)
      ("github.com" ,labhub-url))))

;; Taken and adapted from (guix scripts pull).
(define* (channel-commit-hyperlink channel
                                   #:optional
                                   (commit (channel-commit channel)))
  "Return a hyperlink for COMMIT in CHANNEL, using COMMIT as the hyperlink's
text.  The hyperlink links to a web view of COMMIT, when available."
  (let* ((url  (channel-url channel))
         (uri  (string->uri url))
         (host (and uri (uri-host uri))))
    (if host
        (match (assoc host %vcs-web-views)
          (#f
           commit)
          ((_ template)
           `(a (@ (href ,(template url commit))) ,commit)))
        commit)))
