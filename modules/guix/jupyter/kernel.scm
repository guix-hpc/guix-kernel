;;; Guix-kernel -- Guix kernel for Jupyter
;;; Copyright (C) 2019, 2021, 2024 Ludovic Courtès <ludovic.courtes@inria.fr>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (guix jupyter kernel)
  #:use-module (guix jupyter containers)
  #:use-module (guix jupyter logging)
  #:use-module (jupyter messages)
  #:use-module (jupyter kernels)
  #:use-module (guix gexp)
  #:use-module (guix store)
  #:use-module (guix monads)
  #:use-module (guix modules)
  #:use-module (guix status)
  #:use-module ((guix self) #:select (make-config.scm))
  #:use-module ((guix build utils) #:select (mkdir-p))
  #:use-module ((guix build syscalls) #:select (mkdtemp!))
  #:use-module ((gnu packages) #:select (specification->package))
  #:use-module (gnu system file-systems)
  #:use-module ((gnu build linux-container) #:select (%namespaces))
  #:use-module (srfi srfi-1)
  #:use-module (ice-9 match)
  #:use-module (sxml simple)
  #:use-module (json parser)
  #:use-module (json builder)
  #:export (%language-info
            %kernel-info-reply

            spawn-kernel/container

            with-build-progress-report))

;;; Commentary:
;;;
;;; This contains stuff about the Guix kernel proper.
;;;
;;; Code:

(define %language-info
  ;; Language info for the Guix kernel.
  (language-info
   (name "guile")
   (version (effective-version))
   (mime-type "application/x-scheme")
   (file-extension ".scm")
   (pygments-lexer "scheme")
   (codemirror-mode "scheme")))

(define %kernel-info-reply
  ;; Reply to "kernel_info_request" messages.
  (kernel-info-reply
   (implementation "Guix Jupyter Kernel")
   (implementation-version "0.0.1")
   (language-info %language-info)
   (banner "Guix Kernel")
   (help-links
    '(("Inria GitLab" . "https://gitlab.inria.fr/guix-hpc/guix-kernel")
      ("GNU Guix" . "https://guix.gnu.org")))))


;;;
;;; Running a kernel in a container.
;;;

(define (module-to-import? name)
  "Return true if NAME (a list of symbols) denotes a module that should be
imported."
  (match name
    (('guix 'config) #f)
    (('guix _ ...) #t)
    (('gnu _ ...) #t)
    (('jupyter _ ...) #t)
    (_ #f)))

(define (make-container-directory)
  "Make a temporary directory for use in a container and return its name.
The returned directory contains a 'root' and a 'home' sub-directory meant to
be used respectively as the root and home directory of the container."
  (define parent
    (let ((template (string-append (or (getenv "TMPDIR") "/tmp")
                                   "/guix-jupyter.XXXXXX")))
      (mkdtemp! template)))

  (mkdir-p parent)
  (let* ((template  (string-append parent "/container.XXXXXX"))
         (directory (mkdtemp! template)))
    (mkdir (string-append directory "/root"))
    (mkdir (string-append directory "/home"))
    directory))

(define (spawn-kernel/container context profile)
  "Spawn the kernel found in PROFILE in a new container.  Return, as a
monadic value, a <kernel> connected to that process."
  (define guile-gcrypt
    (specification->package "guile-gcrypt"))
  (define guile-json
    (specification->package "guile-json"))
  (define guile-simple-zmq
    (specification->package "guile-simple-zmq"))

  (define (spawn profile)
    (with-extensions (list guile-gcrypt guile-json guile-simple-zmq)
      (with-imported-modules `(,@(source-module-closure
                                  '((guix profiles)
                                    (guix search-paths)
                                    (jupyter kernels)
                                    (jupyter guile))
                                  #:select? module-to-import?)
                               ((guix config) => ,(make-config.scm)))
        #~(begin
            (use-modules (guix profiles)
                         (guix search-paths)
                         (jupyter kernels)        ;json->connection
                         (jupyter guile)
                         (ice-9 match))

            ;; (set-network-interface-up "lo")  ;up lo interface

            ;; Do like 'guix environment'.
            (setenv "GUIX_ENVIRONMENT" #$profile)

            ;; Make sure kernels can always be found.
            (setenv "JUPYTER_PATH"
                    #$(file-append profile "/share/jupyter"))

            ;; Better feel at home.
            (setenv "HOME" "/home/jupyter")
            (chdir "/home/jupyter")

            ;; Set the environment variables that apply to PROFILE.
            (for-each (match-lambda
                        ((spec . value)
                         (setenv (search-path-specification-variable spec)
                                 value)))
                      (profile-search-paths #$profile))

            (let ((str #$(scm->json-string (connection->json connection))))
              (match (available-kernel-specs #$profile
                                             (list (getenv "JUPYTER_PATH")))
                ((specs)
                 (format (current-error-port)
                         "starting kernel for '~a'...~%"
                         (kernel-specs-display-name specs))
                 (exec-kernel specs (json->connection str)))
                (()
                 #f)))))))

  (define-values (connection kernel)
    (allocate-connection context "tcp" "127.0.0.1"
                         (generate-key)))

  (define (mounts home)
    (cons (file-system
            (device home)
            (mount-point "/home/jupyter")
            (type "none")
            (check? #f)
            (flags '(bind-mount)))
          %container-file-systems))

  (define namespaces
    ;; XXX: Since we'll talk to KERNEL over TCP/IP (due to the fact that we
    ;; use a "connection file" above), this process must live in the global
    ;; network namespace.  TODO: Arrange to use Unix-domain sockets instead.
    (delq 'net %namespaces))

  (mlet* %store-monad ((root ((lift0 make-container-directory %store-monad)))
                       (home -> (string-append root "/home"))
                       (pid  (eval/container* (spawn profile)
                                              (string-append root "/root")
                                              #:mounts (mounts home)
                                              #:namespaces namespaces
                                              #:guest-uid 1000
                                              #:guest-gid 1000)))
    (return (set-kernel-properties (set-kernel-pid kernel pid)
                                   `((home . ,home)
                                     (directory . ,root))))))


;;;
;;; Reporting build events.
;;;

(define (build-event-reporter kernel message display-id)
  "Return build event report procedure suitable for 'with-status-report'."
  (define (report-event shtml)
    (send-message kernel
                  (reply message "update_display_data"
                         (scm->json-string
                          `(("data"
                             . (("text/html"
                                 . ,(sxml->xml-string shtml))))
                            ("metadata" . ())
                            ("transient"
                             . (("display_id" . ,display-id))))))
                  #:kernel-socket kernel-iopub))

  (lambda (event old-status new-status)
    (match event
      (('build-started drv . _)
       (format/log "build started: ~a~%" drv)
       (report-event `(div "Building " (code ,drv) "...")))
      (('download-started item . _)
       (format/log "download started: ~a~%" item)
       (report-event `(div "Downloading " (code ,item) "...")))
      (_
       #f))))

(define (call-with-build-progress-report kernel message thunk)
  (define display-id
    (gensym "progress"))

  ;; Send an initial "display_data" message with DISPLAY-ID.
  (send-message kernel
                (reply message "display_data"
                       (scm->json-string
                        `(("data"
                           . (("text/html"
                               . ,(sxml->xml-string
                                   `(div "Preparing things...")))))
                          ("metadata" . ())
                          ("transient"
                           . (("display_id" . ,display-id))))))
                #:kernel-socket kernel-iopub)

  ;; From then on, send "update_display_data" messages with DISPLAY-ID.
  (let ((result (with-status-report (build-event-reporter kernel message
                                                          display-id)
                  (thunk))))
    (send-message kernel
                  (reply message "update_display_data"
                         (scm->json-string
                          `(("data"
                             . (("text/html" . "Ready.")))
                            ("metadata" . ())
                            ("transient"
                             . (("display_id" . ,display-id))))))
                  #:kernel-socket kernel-iopub)
    result))

(define-syntax-rule (with-build-progress-report kernel message exp ...)
  "Evaluate EXP in a context where build events are reported to KERNEL as a
reply to MESSAGE."
  (call-with-build-progress-report kernel message (lambda () exp ...)))
