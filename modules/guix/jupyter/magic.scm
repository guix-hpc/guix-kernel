;;; Guix-kernel -- Guix kernel for Jupyter
;;; Copyright (C) 2018  Pierre-Antoine Rouby <pierre-antoine.rouby@inria.fr>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (guix jupyter magic)
  #:export (%magic-separator

            magic?
            get-magic-line

            magic-attr?
            magic-env?))

(define %magic-separator "<-")

(define (magic? code)
  "Return true if code begin by magic command like ';;guix ...', else
return false"
  (equal? (car (string-split code #\ ))
          ";;guix"))

(define (get-magic-line code)
  "Return first line of code if is magic line, else return false."
  (if (magic? code)
      (car (string-split code #\newline))
      #f))

(define (magic-attr? code attr)
    (if (magic? code)
      (let ((magic-line (get-magic-line code)))
        (equal? (car (cdr (string-split magic-line #\ )))
                attr))
      #f))

(define (magic-env? code)               ;;;guix environment ...
  (magic-attr? code "environment"))
