;;; Guix-kernel -- Guix kernel for Jupyter
;;; Copyright (C) 2019, 2021 Ludovic Courtès <ludovic.courtes@inria.fr>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (guix jupyter proxy)
  #:use-module (guix jupyter logging)
  #:use-module (jupyter messages)
  #:use-module (jupyter kernels)
  #:use-module (jupyter servers)
  #:use-module (json parser)
  #:use-module (json builder)
  #:use-module (gcrypt base16)
  #:use-module ((guix build utils) #:select (delete-file-recursively))
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-9 gnu)
  #:use-module (ice-9 vlist)
  #:use-module (ice-9 match)
  #:export (proxy-state
            proxy-state?
            proxy-state-client
            proxy-state-client-id
            (proxy-state-proxied/alist . proxy-state-proxied)
            proxy-state-execution-count

            set-proxy-state-client-id
            register-proxied
            unregister-proxied
            lookup-proxied
            proxy-state-proxied-number

            increment-execution-count
            set-proxy-state-execution-count

            proxy-state-property
            set-proxy-state-property

            terminate-proxied-kernel
            terminate-proxied-kernels
            proxy-request-handler))

;;; Commentary:
;;;
;;; This module provides supporting code to implement a kernel server that
;;; proxies a bunch of kernels, the "proxied".
;;;
;;; Code:

(define-immutable-record-type <proxy-state>
  (%proxy-state client client-id proxied count properties)
  proxy-state?
  (client     proxy-state-client set-proxy-state-client)       ;<kernel>
  (client-id  proxy-state-client-id set-proxy-state-client-id) ;bytevector
  (proxied    proxy-state-proxied set-proxy-state-proxied)     ;vhash
  (count      proxy-state-execution-count                      ;integer
              set-proxy-state-execution-count)
  (properties proxy-state-properties              ;alist
              set-proxy-state-properties))

(define* (proxy-state client #:key client-id)
  "Return a new proxy state with CLIENT, a <kernel>, as its client."
  (%proxy-state client client-id vlist-null 0 '()))

(define (proxy-state-proxied/alist state)
  "Return the proxied kernels in STATE as a list of name/kernel pairs."
  (vlist->list (proxy-state-proxied state)))

(define (register-proxied name kernel state)
  "Register KERNEL as a proxied kernel with the given NAME in STATE, a
<proxy-state> record."
  (let ((table (vhash-cons name kernel
                           (proxy-state-proxied state))))
    (set-proxy-state-proxied state table)))

(define (unregister-proxied name state)
  "Unregister the kernel associated with the given NAME, a string, from
STATE's list of proxied kernels."
  (let ((table (vhash-delete name (proxy-state-proxied state))))
    (set-proxy-state-proxied state table)))

(define (lookup-proxied name state)
  "Return the proxied kernel NAME from STATE, or #f if it could not be
found."
  (match (vhash-assoc name (proxy-state-proxied state))
    (#f #f)
    ((_ . kernel) kernel)))

(define (proxy-state-proxied-number state)
  "Return the number of proxied kernels in STATE."
  (vlist-length (proxy-state-proxied state)))

(define (increment-execution-count state)
  "Return STATE with its execution count incremented."
  (let ((count (proxy-state-execution-count state)))
    (set-proxy-state-execution-count state (+ 1 count))))

(define (remove-kernel-directory kernel)
  "Return the directory used to store KERNEL's root and home directories, if
any."
  ;; Proxied kernels running in a container have their 'directory' property
  ;; set in 'spawn-kernel/container'.
  (match (assq-ref (kernel-properties kernel) 'directory)
    (#f #t)
    ((? string? directory)
     (format/log "deleting '~a' of kernel ~a~%"
                 directory (kernel-pid kernel))
     (delete-file-recursively directory))))

(define (terminate-proxied-kernel kernel)
  "Send KERNEL a \"shutdown_request\" and terminate its process."
  (define shutdown
    (message (header "shutdown_request" "user" "1234") ;FIXME
             (scm->json-string '((restart . #f)))))

  (format/log "terminating proxy with PID ~s...~%"
              (kernel-pid kernel))
  (send-message kernel shutdown
                #:kernel-socket kernel-control)
  (close-kernel kernel)
  (usleep 500000)
  (catch 'system-error
    (lambda ()
      (kill (kernel-pid kernel) SIGTERM))
    (const #t))
  (remove-kernel-directory kernel))

(define (terminate-proxied-kernels message state)
  "Terminate all the proxies listed in the CONTAINERS vhash, sending them
MESSAGE.  Return the new state."
  (vlist-for-each (match-lambda
                    ((name . proxy)
                     (format/log "terminating proxy ~s (PID ~s)...~%"
                                 name (kernel-pid proxy))
                     ;; Shutdown messages are sent on the control socket.
                     (send-message proxy message
                                   #:kernel-socket kernel-control)
                     (unmonitor-client proxy)
                     (close-kernel proxy)
                     (false-if-exception (kill (kernel-pid proxy) SIGTERM))
                     (remove-kernel-directory proxy)))
                  (proxy-state-proxied state))
  (set-proxy-state-proxied state vlist-null))

(define (proxy-request-handler handlers)
  "Return a handler procedure suitable for 'serve-kernels' that forwards
messages from the client to the proxied kernels and vice-versa and return the
updated <proxy-state>.  When a client message has one of the types found in
HANDLERS, a list of message type/handler pairs, pass it to that handler."
  (lambda (kernel kind message state)
    (if (eq? kernel (proxy-state-client state))

        ;; Record the socket identity of our client so we can forward shell
        ;; messages to it.  As explained at
        ;; <http://zguide.zeromq.org/php:chapter3#header-68>, we get to
        ;; discover the identity of our peer the first time we get a message
        ;; from it, which is why we record it here.
        (let* ((client-id (message-sender message))
               (state     (set-proxy-state-client-id state client-id))
               (handler   (assoc-ref handlers (message-type message))))
          (if handler
              (with-busy-section kernel message
                (handler kernel kind message state))
              (begin
                (format/log "unhandled '~a' message from client; ignoring~%"
                            (message-type message))
                state)))

        ;; This message is coming from one of our proxied kernels, so
        ;; forward it to our client.
        (let ((id (proxy-state-client-id state)))
          (format/log "forwarding ~s from ~s, socket ~s to ~s~%"
                      (message-type message) (kernel-pid kernel)
                      kind (bytevector->base16-string id))
          (send-message (proxy-state-client state) message
                        #:kernel-socket kind
                        #:recipient id)
          state))))

(define* (proxy-state-property state key #:optional default)
  "Return the property associated with KEY in STATE, or DEFAULT if it could
not be found."
  ;; This is where we break into a property of the state.
  (match (assq key (proxy-state-properties state))
    (#f default)
    ((_ . value) value)))

(define (set-proxy-state-property state key value)
  "Record the KEY/VALUE association in STATE's properties."
  (let ((properties (proxy-state-properties state)))
    (set-proxy-state-properties state
                                `((,key . ,value)
                                  ,@(alist-delete key properties eq?)))))
