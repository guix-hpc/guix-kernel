;;; Guix-kernel -- Guix kernel for Jupyter
;;; Copyright (C) 2019, 2021, 2024 Ludovic Courtès <ludovic.courtes@inria.fr>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (jupyter guile)
  #:use-module (jupyter messages)
  #:use-module (jupyter kernels)
  #:use-module (jupyter servers)
  #:use-module (json parser)
  #:use-module (json builder)
  #:use-module (sxml simple)
  #:use-module (simple-zmq)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)
  #:use-module (srfi srfi-71)
  #:use-module (ice-9 match)
  #:use-module (ice-9 rdelim)
  #:autoload   (ice-9 threads) (call-with-new-thread join-thread)
  #:export (run-guile-kernel
            available-kernel-specs))

;;; Commentary:
;;;
;;; This module provides a Jupyter kernel for Guile programming.
;;;
;;; Code:



;;;
;;; Logging.
;;;

(define current-logging-port
  ;; Logging port, for debugging.
  (make-parameter (current-error-port)))

(define-syntax format/log
  (lambda (s)
    "Log the given string."
    (syntax-case s ()
      ((_ fmt args ...)
       (string? (syntax->datum #'fmt))
       (with-syntax ((fmt (string-append "guile kernel[~a]: "
                                         (syntax->datum #'fmt))))
         #'(format (current-logging-port) fmt
                   (getpid) args ...))))))


;;;
;;; Kernel info.
;;;

(define %language-info
  ;; Language info for the Guix kernel.
  (language-info
   (name "guile")
   (version (effective-version))
   (mime-type "application/x-scheme")
   (file-extension ".scm")
   (pygments-lexer "scheme")
   (codemirror-mode "scheme")))

(define %kernel-info-reply
  ;; Reply to "kernel_info_request" messages.
  (kernel-info-reply
   (implementation "GNU Guile")
   (implementation-version "0.0.1")
   (language-info %language-info)
   (banner "GNU Guile kernel")
   (help-links
    '(("Inria GitLab" . "https://gitlab.inria.fr/guix-hpc/guix-kernel")
      ("GNU Guix" . "https://guix.gnu.org")
      ("GNU Guile" . "https://www.gnu.org/software/guile/")))))


;;;
;;; Request handling.
;;;

(define (reply-kernel-info-request kernel kind message state)
  (send-message kernel
                (reply message "kernel_info_reply"
                       (scm->json-string
                        (kernel-info-reply->json %kernel-info-reply))))
  state)

(define %user-module
  ;; Module in which user code runs.
  (let ((module (resolve-module '(jupyter-user) #f #f #:ensure #t)))
    (beautify-user-module! module)
    module))

(define (frame->string frame)        ;TODO: Improve by showing location, etc.
  (match (frame-procedure-name frame)
    (#f "?")
    (name (symbol->string name))))

(define (code->execute-reply code counter)
  "Return two values: an <execute-reply> record, and a Scheme value resulting
from the evaluation of CODE, a string."
  (define prompt
    (make-prompt-tag "user-extent"))

  (define stack #f)

  (catch #t
    (lambda ()
      (let ((result (call-with-prompt prompt
                      (lambda ()
                        (eval (with-input-from-string code read)
                              %user-module))
                      (const #f))))
        (values (execute-reply (status 'ok)
                               (counter counter))
                result)))
    (lambda (key . args)
      (let ((frames (unfold (cute >= <> (stack-length stack))
                            (cut stack-ref stack <>)
                            1+ 0)))
        (values (execute-reply (status 'error)
                               (counter counter)
                               (exception-name (symbol->string key))
                               (exception-value (object->string args))
                               (traceback (map frame->string frames)))
                #f)))
    (lambda (key . args)                          ;pre-unwind hook
      (set! stack (make-stack #t 1 prompt)))))

(define (picture? obj)
  "Return true if OBJ looks like a picture coming from the Guile Picture
Language."
  (and (record? obj)
       (eq? '<pict> (record-type-name (record-type-descriptor obj)))
       (match (struct-ref obj 0)
         (('svg . _) #t)
         (_ #f))))

(define (picture->ssvg pict)
  "Return SVG/SXML for PICT, a picture from the Picture Language."
  (struct-ref pict 0))

(define (reply-execute-request kernel kind message counter)
  (define (forward-output output-port error-port)
    ;; Read from OUTPUT-PORT and ERROR-PORT and forward individual lines read
    ;; from them as "stream" messages sent to KERNEL.  Return when the
    ;; end-of-file has been reached on OUTPUT-PORT and ERROR-PORT.
    (format/log "forwarding stdout from ~s and stderr from ~s~%"
                output-port error-port)
    (setvbuf output-port 'line)
    (setvbuf error-port 'line)
    (let loop ((ports (list output-port error-port)))
      (match (select ports '() '())
        ((() () ())
         (loop ports))
        (((port _ ...) () ())
         (match (read-line port 'concat)
           ((? eof-object?)
            (format/log "done forwarding stdout/stderr from ~s~%"
                        port)
            (close-port port)
            (match (delq port ports)
              (() #t)
              (ports (loop ports))))
           (""                            ;no need for a "stream" message here
            (loop ports))
           (str
            (send-message kernel
                          (reply message "stream"
                                 (scm->json-string
                                  (stream->json
                                   (stream (name (if (eq? port output-port)
                                                     'stdout
                                                     'stderr))
                                           (text str)))))
                          #:kernel-socket kernel-iopub)
            (loop ports)))))))

  (define (call-with-output-capture thunk)
    ;; Invoke THUNK in a separate thread, relaying its output and error ports
    ;; to Jupyter.
    (let ((stdout (pipe))
          (stderr (pipe)))
      (define thread
        ;; XXX: We could avoid creating a new thread for each execution
        ;; request, but it's not clear that it's worth it.
        (call-with-new-thread
         (lambda ()
           (define (finish)
             (close-port (cdr stdout))
             (close-port (cdr stderr)))

           (catch #t
             (lambda ()
               (define result
                 (parameterize ((current-output-port (cdr stdout))
                                (current-error-port (cdr stderr)))
                   (setvbuf (current-output-port) 'none)
                   (setvbuf (current-error-port) 'none)
                   (call-with-values thunk
                     (lambda results
                       `(values ,@results)))))
               (finish)
               result)
             (lambda args
               (finish)
               `(exception ,@args))))))

      (forward-output (car stdout) (car stderr))
      (match (join-thread thread)
        (('values results ...)
         (apply values results))
        (('exception key args ...)
         (apply throw key args)))))

  (define-syntax-rule (with-output-capture exp)
    (call-with-output-capture (lambda () exp)))

  (format/log "handling execute-request~%")
  (let* ((request (json->execute-request (message-content message)))
         (sender  (header-sender (message-header message)))
         (code    (string-append "(begin "
                                 (execute-request-code request )
                                 ")")))
    ;; First off, say we're busy and echo the input back.
    (pub-busy kernel message)
    (send-message kernel
                  (reply message "execute_input"
                         (scm->json-string
                          (execute-input->json
                           (execute-input (code code)
                                          (counter counter)))))
                  #:kernel-socket kernel-iopub)

    (let ((xreply result (with-output-capture
                          (code->execute-reply code counter))))

      (format/log "sending execution result for ~s~%" code)

      (if (eq? 'error (execute-reply-status xreply))
          (send-message kernel
                        (reply message "error"
                               (scm->json-string
                                `(("ename"
                                   . ,(execute-reply-exception-name xreply))
                                  ("evalue"
                                   . ,(execute-reply-exception-value xreply))
                                  ("traceback"
                                   . ,(list->vector
                                       (execute-reply-traceback xreply))))))
                        #:kernel-socket kernel-iopub)
          (send-message kernel
                        (reply message "execute_result"
                               (scm->json-string
                                (execute-result->json
                                 (execute-result
                                  (counter counter)
                                  (data
                                   (if (picture? result)
                                       `(("image/svg+xml"
                                          . ,(sxml->xml-string
                                              (picture->ssvg result))))
                                       `(("text/plain"
                                          . ,(object->string result)))))))))
                        #:kernel-socket kernel-iopub))

      (send-message kernel
                    (reply message "execute_reply"
                           (scm->json-string
                            (execute-reply->json xreply)))
                    #:recipient (message-sender message)
                    #:kernel-socket kernel-shell)
      (pub-idle kernel message)
      (+ 1 counter))))

(define %scheme-symbol-char-set
  ;; Charset of Scheme symbols, loosely inspired by the "Lexical structure"
  ;; section in R5RS.
  (char-set-difference char-set:graphic
                       (char-set #\( #\) #\[ #\] #\" #\;)))

(define (symbol-at-point str cursor)
  "Return the Scheme symbol prefix immediately preceding index CURSOR in STR."
  (let loop ((index cursor)
             (result '()))
    (let ((chr (string-ref str index)))
      (if (and (>= index 0)
               (char-set-contains? %scheme-symbol-char-set chr))
          (loop (- index 1) (cons chr result))
          (list->string result)))))

(define (bindings-with-prefix module prefix)
  "Return, as a list of strings, global bindings of MODULE and its imported
modules whose name starts with PREFIX."
  (define lst
    (let loop ((module module))
      (append (hash-fold (lambda (binding variable result)
                           (let ((binding (symbol->string binding)))
                             (if (string-prefix? prefix binding)
                                 (cons binding result)
                                 result)))
                         '()
                         (module-obarray module))
              (append-map loop (module-uses module)))))

  (sort (delete-duplicates lst) string<?))

(define (reply-complete-request kernel kind message state)
  "Reply to a \"complete_request\" message--i.e., a completion request.
Return STATE."
  (define (send-completion-reply matches start end)
    (format/log "sending completion reply with ~a matches~%"
                (length matches))
    (send-message kernel
                  (reply message "complete_reply"
                         (scm->json-string
                          (complete-reply->json
                           (complete-reply (status 'ok)
                                           (matches matches)
                                           (cursor-start start)
                                           (cursor-end end)))))
                  #:recipient (message-sender message)))

  (let* ((request (json->complete-request (message-content message)))
         (code    (complete-request-code request))
         (cursor  (complete-request-cursor-position request)))
    (match (symbol-at-point code (- cursor 1))
      ("" #f)
      (symbol
       (let ((matches (bindings-with-prefix %user-module symbol)))
         (format/log "found ~a global bindings prefixed by ~s in ~a~%"
                     (length matches) symbol (module-name %user-module))
         (send-completion-reply matches
                                (- cursor (string-length symbol))
                                cursor))))
    state))

(define (shutdown kernel kind message state)
  (format/log "shutting down~%")
  (leave-server-loop state))

;;
;; Dispatch route.
;;

(define dispatch-route
  `(("kernel_info_request" . ,reply-kernel-info-request)
    ("execute_request"     . ,reply-execute-request)
    ("complete_request"    . ,reply-complete-request)
    ("shutdown_request"    . ,shutdown)))

(define (request-handler handlers)
  "Return a request handler that dispatches to HANDLERS, an alist."
  (lambda (kernel kind message state)
    (let ((handler (assoc-ref handlers (message-type message))))
      (if handler
          (handler kernel kind message state)
          (begin
            (format/log "unhandled '~a' message from client; ignoring~%"
                        (message-type message))
            state)))))


;;
;; Top level.
;;

(define (run-guile-kernel connection)
  "Run a Guile kernel with its client at CONNECTION, a <connection> record.
Return the kernel's state upon receiving a 'shutdown' request."
  (let* ((context (zmq-create-context))
         (kernel  (connection->kernel connection #:context context)))
    (format/log "started Guile kernel as PID ~a~%" (getpid))
    (let ((result (serve-kernels (list kernel)
                                 (request-handler dispatch-route)
                                 0)))
      (close-kernel kernel)
      result)))


(define (guile-kernel-specs guile)
  "Return a <kernel-specs> record running the built-in Guile kernel with
GUILE, the file name of the 'guile' executable."
  (define (directory path file)
    (fold (lambda (_ file)
            (dirname file))
          (search-path path file)
          (string-tokenize file (char-set-complement (char-set #\/)))))

  (kernel-specs
   (display-name "GNU Guile")
   (language "Guile Scheme")
   (arguments `(,guile

                ;; This beautiful hack allows us to get our dependencies.
                "-L" ,(directory %load-path "jupyter/guile.scm")
                "-L" ,(directory %load-path "gcrypt/mac.scm")
                "-L" ,(directory %load-path "json.scm")
                "-L" ,(directory %load-path "simple-zmq.scm")
                "-C" ,(directory %load-compiled-path "jupyter/guile.go")
                "-C" ,(directory %load-compiled-path "gcrypt/mac.go")
                "-C" ,(directory %load-compiled-path "json/builder.go")
                "-C" ,(directory %load-compiled-path "simple-zmq.go")

                "-c"
                ,(object->string
                  '(begin
                     (use-modules (jupyter guile)
                                  (jupyter kernels))

                     (call-with-input-file (car (last-pair (command-line)))
                       (compose run-guile-kernel json->connection))))
                "--"
                "{connection_file}"))))

(define* (available-kernel-specs profile
                                 #:optional (path (jupyter-kernel-path)))
  "Return the list of <kernel-specs> found in PATH, or a spec for the
built-in Guile kernel if PROFILE contains a 'guile' executable, or #f."
  (define specs
    (map (lambda (file)
           (call-with-input-file file json->kernel-specs))
         (available-kernel-specs-files path)))

  (match specs
    (()
     (let ((guile (string-append profile "/bin/guile")))
       (if (file-exists? guile)
           (list (guile-kernel-specs guile))
           '())))
    (specs
     specs)))
