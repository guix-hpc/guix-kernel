;;; Guix-kernel -- Guix kernel for Jupyter
;;; Copyright (C) 2018, 2019, 2021 Ludovic Courtès <ludo@gnu.org>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (jupyter json)
  #:use-module (json parser)
  #:use-module (guix records)
  #:export (json
            <=>
            define-json-mapping))

;;; Commentary:
;;;
;;; Helpers to map JSON objects to records.  Taken from (guix swh).
;;; Guile-JSON 4.x includes a variant of this code.  The difference is that
;;; this module builds upon the 'define-record-type*' feature of (guix
;;; records), whereas Guile-JSON builds on top of SRFI-9.
;;;
;;; Consequently, the syntax is a bit different; in particular, this variant
;;; uses the 'json' keyword to distinguish parameters of the JSON mapping
;;; from other properties of record fields, such as default values.
;;;
;;; Code:

;; Literals.
(define json 'json)
(define <=> '<=>)

(define-syntax-rule (define-json-reader json->record ctor spec ...)
  "Define JSON->RECORD as a procedure that converts a JSON representation,
read from a port, string, or hash table, into a record created by CTOR and
following SPEC, a series of field specifications."
  (define (json->record input)
    (let ((table (cond ((port? input)
                        (json->scm input))
                       ((string? input)
                        (json-string->scm input))
                       ((or (null? input) (pair? input))
                        input))))
      (let-syntax ((extract-field (syntax-rules (json)
                                    ((_ table (field (json key json->value
                                                           _ (... ...))
                                                     _ (... ...)))
                                     (json->value (assoc-ref table key)))
                                    ((_ table (field (json key)
                                                     _ (... ...)))
                                     (assoc-ref table key))
                                    ((_ table (field _ (... ...)))
                                     (assoc-ref table
                                                (symbol->string 'field))))))
        (ctor (extract-field table spec) ...)))))

(define-syntax-rule (define-json-writer record->json
                      (field-spec ...) ...)
  "Define RECORD->JSON as a procedure that returns an alist, the Guile-JSON
representation of the given record."
  (define (record->json record)
    (let-syntax ((field->alist (syntax-rules (json)
                                 ((_ field getter
                                     (json key json->value value->json)
                                     _ (... ...))
                                  (cons key
                                        (value->json (getter record))))
                                 ((_ field getter
                                     (json key _ (... ...))
                                     _ (... ...))
                                  (cons key
                                        (getter record)))
                                 ((_ field getter _ (... ...))
                                  (cons (symbol->string 'field)
                                        (getter record))))))
      (list (field->alist field-spec ...) ...))))

(define-syntax define-json-mapping
  (syntax-rules (<=>)
    "Define RTD as a record type with the given FIELDs and GETTERs, à la SRFI-9,
and define JSON->RECORD as a conversion from JSON to a record of this type.
Optionally, define RECORD->JSON as the conversion from a record of this type
to its JSON representation (an alist).

This is layered on top of 'define-record-type*'.  Here's an example:

  (define-json-mapping <foo> foo make-foo
    foo?
    json->foo <=> foo->json
    (a foo-a (json \"A\" string->number number->string)
       (default 2))
    (b foo-b))

"
    ((_ rtd ctor ctor-proc
        pred json->record <=> record->json
        (field getter spec ...) ...)
     (begin
       (define-json-mapping rtd ctor ctor-proc
         pred json->record
         (field getter spec ...) ...)

       (define-json-writer record->json
         (field getter spec ...) ...)))
    ((_ rtd ctor ctor-proc pred json->record
        (field getter spec ...) ...)
     (begin
       (define-record-type* rtd ctor ctor-proc
         pred
         (field getter spec ...) ...)

       (define-json-reader json->record ctor-proc
         (field spec ...) ...)))))
