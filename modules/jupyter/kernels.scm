;;; Guix-kernel -- Guix kernel for Jupyter
;;; Copyright (C) 2018, 2019 Pierre-Antoine Rouby <pierre-antoine.rouby@inria.fr>
;;; Copyright (C) 2018, 2019, 2021 Ludovic Courtès <ludovic.courtes@inria.fr>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (jupyter kernels)
  #:use-module (jupyter messages)
  #:use-module (jupyter json)
  #:use-module (guix build utils)
  #:use-module ((guix build syscalls) #:select (mkdtemp!))
  #:use-module (gcrypt mac)
  #:use-module (gcrypt base64)
  #:use-module (ice-9 rdelim)
  #:use-module (ice-9 match)
  #:use-module (ice-9 ftw)
  #:use-module (simple-zmq)
  #:use-module (json parser)
  #:use-module (json builder)
  #:use-module (sxml simple)
  #:use-module (rnrs bytevectors)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-9 gnu)
  #:use-module (srfi srfi-26)
  #:use-module (srfi srfi-71)
  #:export (jupyter-kernel-path
            find-kernel-specs
            find-kernel-specs-file
            available-kernel-specs-files
            exec-kernel
            spawn-kernel
            run-kernel

            kernel-specs?
            kernel-specs
            kernel-specs-arguments
            kernel-specs-display-name
            kernel-specs-language
            json->kernel-specs
            kernel-specs->json

            kernel
            kernel?
            kernel-name
            kernel-pid
            kernel-key
            kernel-control
            kernel-shell
            kernel-standard-input
            kernel-heartbeat
            kernel-iosub
            kernel-iopub                          ;alias
            kernel-sockets
            kernel-socket-kind
            kernel-properties
            set-kernel-name
            set-kernel-pid
            set-kernel-properties

            connection?
            connection-transport
            connection-ip
            connection-signature-scheme
            connection-key
            connection-control-port
            connection-shell-port
            connection-stdin-port
            connection-heartbeat-port
            connection-iopub-port
            json->connection
            connection->json
            generate-key
            allocate-connection

            zmq-poll*                             ;XXX: temporary hack
            read-message
            send-message
            with-busy-section
            pub-busy
            pub-idle

            sxml->xml-string
            reply-shtml

            close-kernel))

;;; Commentary:
;;;
;;; This module implements Jupyter "kernels" as defined at
;;; <https://jupyter-client.readthedocs.io/en/latest/kernels.html>.
;;;
;;; Code:

;;
;; Kernel execution.
;;

;; Type of a Jupyter kernel.
(define-immutable-record-type <kernel>
  (%kernel name pid key control shell stdin heartbeat iosub
           properties)
  kernel?
  (name        kernel-name set-kernel-name)       ;string
  (pid         kernel-pid set-kernel-pid)         ;integer
  (key         kernel-key)                        ;bytevector
  (control     kernel-control)                    ;zmq socket
  (shell       kernel-shell)                      ;zmq socket
  (stdin       kernel-standard-input)             ;zmq socket
  (heartbeat   kernel-heartbeat)                  ;zmq socket
  (iosub       kernel-iosub)                      ;zmq socket
  (properties  kernel-properties                  ;alist
               set-kernel-properties))

(define-syntax kernel-iopub (identifier-syntax kernel-iosub))

;; Kernel metadata taken from a 'kernel.json' file.
;; <https://jupyter-client.readthedocs.io/en/latest/kernels.html#kernel-specs>
(define-json-mapping <kernel-specs>
  kernel-specs make-kernel-specs kernel-specs?
  json->kernel-specs <=> kernel-specs->json
  (arguments      kernel-specs-arguments
                  (json "argv" vector->list list->vector))
  (display-name   kernel-specs-display-name
                  (json "display_name"))
  (language       kernel-specs-language))

(define* (kernel name pid #:key key control shell
                 standard-input heartbeat iosub (properties '()))
  (%kernel name pid key
           control shell standard-input heartbeat iosub
           properties))

(define (kernel-sockets kernel)
  "Return all the ZeroMQ sockets associated with KERNEL."
  (map (lambda (socket)
         (socket kernel))
       (list kernel-shell
             kernel-control
             kernel-standard-input
             kernel-heartbeat
             kernel-iopub)))

(define (kernel-socket-kind kernel socket)
  "Return the procedure (e.g., 'kernel-shell', 'kernel-control') that, when
applied on KERNEL, returns SOCKET.  This allows the caller to determine the
purpose of SOCKET for KERNEL.

Return #f if SOCKET is not one of KERNEL's sockets."
  (find (lambda (kernel-socket)
          (eq? (kernel-socket kernel) socket))
        (list kernel-shell
              kernel-control
              kernel-standard-input
              kernel-heartbeat
              kernel-iopub)))

(define (close-kernel kernel)
  "Close all the open connections of KERNEL."
  (for-each zmq-close-socket (kernel-sockets kernel)))

(define jupyter-kernel-path
  ;; Default search path for Jupyter kernels.
  (make-parameter (or (and=> (getenv "JUPYTER_PATH")
                             (cut string-split <> #\:))
                      '())))

(define* (find-kernel-specs kernel
                            #:optional (path (jupyter-kernel-path)))
  "Return the kernel specs for KERNEL, or #f if KERNEL could not be found in
PATH."
  (match kernel
    ("ipython"
     ;; IPython does not provide a 'kernel.json' file like other
     ;; kernels do; instead, we need to execute 'ipython' from $PATH.
     (let ((ipython (search-path (search-path->list (getenv "PATH"))
                                 "ipython")))
       (and ipython
            (kernel-specs
             (arguments (list ipython "kernel" "--quiet"
                              "-f" "{connection_file}"))
             (display-name "IPython")
             (language "Python")))))
    (_
     (let ((file (find-kernel-specs-file kernel)))
       (and file (call-with-input-file file json->kernel-specs))))))

(define* (find-kernel-specs-file kernel
                                 #:optional (path (jupyter-kernel-path)))
  "Return the absolute file name of the 'kernel.json' file for Jupyter kernel
KERNEL, searched for in PATH, a list of directories.  Return #f if KERNEL
could not be found."
  (any (lambda (directory)
         (let ((json (string-append directory "/kernels/"
                                    kernel "/kernel.json")))
           (and (file-exists? json) json)))
       path))

(define* (available-kernel-specs-files #:optional
                                       (path (jupyter-kernel-path)))
  "Return the list of available kernel specs files (the 'kernel.json' files)
found in PATH."
  (define (not-dot? file)
    (not (member file '("." ".."))))

  (append-map (lambda (directory)
                (let* ((directory (string-append directory "/kernels"))
                       (entries   (map (cut string-append directory "/" <>)
                                       (or (scandir directory not-dot?) '()))))
                  (filter-map (lambda (entry)
                                (let ((spec (string-append entry
                                                           "/kernel.json")))
                                  (and (file-exists? spec) spec)))
                              entries)))
              path))


;;
;; Kernel "connection files".
;;

;; A "connection" represents a rendezvous between the client and a kernel:
;; <https://jupyter-client.readthedocs.io/en/stable/kernels.html#connection-files>.
;; Usually the client creates it, writes it to a "connection file", which it
;; passes to the kernel.
(define-json-mapping <connection> connection make-connection connection?
  json->connection <=> connection->json
  (transport         connection-transport)        ;string
  (ip                connection-ip)               ;string
  (signature-scheme  connection-signature-scheme
                     (default "hmac-sha256"))     ;string
  (key               connection-key)              ;string
  (control-port      connection-control-port      ;integer
                     (json "control_port"))
  (shell-port        connection-shell-port        ;integer
                     (json "shell_port"))
  (stdin-port        connection-stdin-port        ;integer
                     (json "stdin_port"))
  (heartbeat-port    connection-heartbeat-port    ;integer
                     (json "hb_port"))
  (iopub-port        connection-iopub-port        ;integer
                     (json "iopub_port")))

(define (generate-key)
  "Return a string usable as a shared secret key between a kernel server and
its clients."
  (base64-encode (generate-signing-key)))

(define* (allocate-connection context transport ip key
                              #:key
                              first-port identity)
  "Allocate ports for TRANSPORT and IP and connect zmq sockets for them under
CONTEXT.  Return a <connection> and a <kernel>: the connection can be passed
to a kernel as its \"connection file\", and the kernel can be used by the
client to talk to it.

When FIRST-PORT is true, use TCP ports from FIRST-PORT on.  When FIRST-PORT
is false, use an OS-allocated port number, which will hopefully not already
be in use (but this is racy)."
  (define (port->uri port)
    (string-append transport "://" ip ":"
                   (number->string port)))

  (define (allocate-port)
    ;; Ask the OS to give us a port number, and return it (jupyter-client
    ;; does the same in 'connect.py'.)  XXX: There's fundamentally a TOCTTOU
    ;; race here because we're on the client side and a server could very
    ;; bind(2) to that port after we've decided to use it, but that's
    ;; inherent to the way the Jupyter protocol works: it's the client that
    ;; chooses ports the kernels should bind to.
    (let ((sock    (socket AF_INET SOCK_STREAM 0))
          (address (make-socket-address AF_INET
                                        (inet-pton AF_INET ip) 0)))
      (setsockopt sock SOL_SOCKET SO_LINGER '(0 . 0))
      (bind sock address)
      (let ((port (sockaddr:port (getsockname sock))))
        (close sock)
        port)))

  (define (try-connect socket port)
    (let ((port (or port (allocate-port))))
      (zmq-connect socket (port->uri port))
      port))

  (let* ((socket-control   (zmq-create-socket context ZMQ_DEALER))
         (socket-shell     (zmq-create-socket context ZMQ_DEALER))
         (socket-stdin     (zmq-create-socket context ZMQ_DEALER))
         (socket-heartbeat (zmq-create-socket context ZMQ_REQ))
         (socket-iosub     (zmq-create-socket context ZMQ_SUB))

         (port-control     (try-connect socket-control   first-port))
         (port-shell       (try-connect socket-shell
                                        (and first-port (+ 1 port-control))))
         (port-stdin       (try-connect socket-stdin
                                        (and first-port (+ 1 port-shell))))
         (port-heartbeat   (try-connect socket-heartbeat
                                        (and first-port (+ 1 port-stdin))))
         (port-iosub       (try-connect socket-iosub
                                        (and first-port (+ 1 port-heartbeat))))

         (connection       (connection
                            (transport transport) (ip ip)
                            (key key)
                            (control-port port-control)
                            (shell-port port-shell)
                            (heartbeat-port port-heartbeat)
                            (stdin-port port-stdin)
                            (iopub-port port-iosub)))

         (kernel (kernel #f #f                    ;no name and PID yet
                         #:key key
                         #:control socket-control
                         #:shell socket-shell
                         #:standard-input socket-stdin
                         #:heartbeat socket-heartbeat
                         #:iosub socket-iosub)))

    (zmq-set-socket-option socket-iosub ZMQ_SUBSCRIBE "")

    ;; <http://zguide.zeromq.org/page:all#The-Extended-Reply-Envelope> says
    ;; that ZeroMQ "generate[s] a 5 byte identity by default", so usually we
    ;; don't need to provide our own identity.
    (when identity
      (zmq-set-socket-option socket-shell ZMQ_IDENTITY
                             (utf8->string identity)))

    (values connection kernel)))

(define (exec-kernel specs connection)
  "Execute the kernel specified by SPECS, a <kernel-specs> record, passing it
CONNECTION serialized to a \"connection file\"."
  (define parent
    (let ((template (string-append (or (getenv "TMPDIR") "/tmp")
                                   "/guix-jupyter-kernel.XXXXXX")))
      (mkdir-p (dirname template))
      (mkdtemp! template)))


  (define connection-file
    (string-append parent "/conn/"
                   (number->string (string-hash
                                    (scm->json-string
                                     (connection->json connection))))
                   ".json"))

  (define arguments
    (map (match-lambda
           ("{connection_file}" connection-file)
           (x x))
         (kernel-specs-arguments specs)))

  (mkdir-p (dirname connection-file))
  (call-with-output-file connection-file
    (lambda (port)
      (scm->json (connection->json connection) port
                 #:pretty #t)))

  (apply execlp (first arguments) arguments))

(define (spawn-kernel specs connection)
  "Spawn the kernel designated by SPECS, a <kernel-specs> record, as a new
process, and return its PID.  Pass it CONNECTION serialized to a \"connection
file\"."
  (let ((pid (primitive-fork)))
    (if (zero? pid)
        (exec-kernel specs connection)
        pid)))

(define search-path->list
  (let ((not-colon (char-set-complement (char-set #\:))))
    (match-lambda
      (#f '())
      ((? string? str)
       (string-tokenize str not-colon)))))

(define* (run-kernel context specs key #:key identity)
  "Spawn the kernel specified by SPECS.  Return a <kernel> record usable by
its client."
  (let ((connection kernel (allocate-connection context "tcp" "127.0.0.1"
                                                key #:identity identity)))
    (set-kernel-pid (set-kernel-name kernel
                                     (kernel-specs-display-name specs))
                    (spawn-kernel specs connection))))


;;
;; Communicating with a kernel.
;;

(define (EINTR-safe proc)
  "Return a variant of PROC that catches EINTR 'zmq-error' exceptions and
retries a call to PROC."
  (define (safe . args)
    (catch 'zmq-error
      (lambda ()
        (apply proc args))
      (lambda (key errno . rest)
        (if (= errno EINTR)
            (apply safe args)
            (apply throw key errno rest)))))

  safe)

(define zmq-poll*
  ;; XXX: As of guile-simple-zmq commit 878f6dc (Nov. 2018), 'zmq-poll'
  ;; doesn't catch EINTR, so do it here.
  (EINTR-safe zmq-poll))

(define* (read-message kernel #:optional (timeout -1))
  "Read one message from one of the sockets of KERNEL and return it.  If the
message is a \"regular\" JSON message, return it as an alist; if it's a
heartbeat message, return it as a bytevector.

If TIMEOUT is -1, wait indefinitely; otherwise wait that number of
milliseconds.  If TIMEOUT expires before a message has been received, return
#f."
  (define shell (kernel-shell kernel))
  (define iopub (kernel-iopub kernel))
  (define items
    (zmq-poll* (map (lambda (socket)
                      (poll-item socket (logior ZMQ_POLLIN ZMQ_POLLERR)))
                    (kernel-sockets kernel))
               timeout))

  (let loop ((items items))
    (match items
      (()
       #f)
      ((item rest ...)
       (let* ((socket (poll-item-socket item))
              (parts  (zmq-get-msg-parts-bytevector socket)))
         ;; Heartbeat messages are raw "bytestrings" that should be echoed
         ;; back right away.
         (if (eq? socket (kernel-heartbeat kernel))
             parts
             (parts->message parts)))))))

(define* (send-message kernel message
                       #:key
                       (kernel-socket kernel-shell)
                       (recipient (and=> (message-parent-header message)
                                         header-sender)))
  "Send message over the shell socket of KERNEL to RECIPIENT, a ZeroMQ
identity (bytevector)."
  (zmq-send-msg-parts-bytevector (kernel-socket kernel)
                                 (message-parts message
                                                (kernel-key kernel)
                                                #:recipient recipient)))

(define (pub kernel message status)
  (let ((content (kernel-status->json status)))
    (send-message kernel (reply message "status"
                                  (scm->json-string content))
                  #:kernel-socket kernel-iopub)))

(define (call-with-busy-section kernel message thunk)
  (dynamic-wind
    (lambda ()
      (pub-busy kernel message))
    thunk
    (lambda ()
      (pub-idle kernel message))))

(define-syntax-rule (with-busy-section kernel message exp ...)
  "Evaluate EXP... after sending a \"busy\" message to KERNEL in reply to
MESSAGE (a request), and send an \"idle\" message upon completion."
  (call-with-busy-section kernel message (lambda () exp ...)))

(define (pub-busy kernel message)
  (pub kernel message
       (kernel-status (execution-state 'busy))))

(define (pub-idle kernel message)
  (pub kernel message
       (kernel-status (execution-state 'idle))))

;;
;; Reply.
;;

(define (sxml->xml-string sxml)
  (call-with-output-string
    (lambda (port)
      (sxml->xml sxml port))))

(define (reply-shtml kernel message shtml count)
  "Reply to MESSAGE with SHTML."
  (let ((code (assoc-ref (json-string->scm (message-content message))
                         "code"))
	(empty-object '())
	(counter      (+ count 1))                ;execution counter

        (html (sxml->xml-string shtml))
        (send (lambda (socket type content)
                (send-message kernel
                              (reply message type
                                     (scm->json-string content))
                              #:kernel-socket socket
                              #:recipient (and (eq? socket kernel-shell)
                                               (message-sender message))))))

    (send kernel-iopub
          "execute_input" `(("code"            . ,code)
                            ("execution_count" . ,counter)))
    (send kernel-iopub
          "execute_result" `(("data"     . (("text/html" . ,html)))
                             ("metadata" . ,empty-object)
                             ("execution_count" . ,counter)))
    (send kernel-shell
          "execute_reply" `(("status"           . "ok")
                            ("execution_count"  . ,counter)
                            ("payload"          . [])
                            ("user_expressions" . ,empty-object)))
    counter))
