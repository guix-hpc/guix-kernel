;;; Guix-kernel -- Guix kernel for Jupyter
;;; Copyright (C) 2018 Evgeny Panfilov <epanfilov@gmail.com>
;;; Copyright (C) 2018-2019, 2021, 2024 Ludovic Courtès <ludo@gnu.org>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (jupyter messages)
  #:use-module (simple-zmq)
  #:use-module (json parser)
  #:use-module (json builder)
  #:use-module (jupyter json)
  #:use-module (gcrypt mac)
  #:use-module (gcrypt base16)
  #:use-module (rnrs bytevectors)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-9 gnu)
  #:use-module (srfi srfi-19)
  #:use-module (ice-9 match)
  #:export (message
            make-message
            reply
            message?
            message-header
            message-parent-header
            message-metadata
            message-content
            message-buffers
            message-type
            message-sender
            message-parts
            set-message-content
            parts->message

            header
            make-header
            header?
            header-id
            header-user
            header-session-id
            header-date
            header-type
            header-version
            header-sender

            kernel-info-reply?
            kernel-info-reply
            kernel-info-reply->json
            json->kernel-info-reply
            kernel-info-reply-language-info
            kernel-info-reply-status
            kernel-info-reply-protocol-version
            kernel-info-reply-implementation
            kernel-info-reply-implementation-version
            kernel-info-reply-language-info
            kernel-info-reply-banner
            kernel-info-reply-help-links

            language-info?
            language-info
            language-info-name
            language-info-version
            language-info-mime-type
            language-info-file-extension
            language-info-pygments-lexer
            language-info-codemirror-mode

            kernel-status?
            kernel-status
            kernel-status-execution-state
            json->kernel-status
            kernel-status->json

            execute-input?
            execute-input
            execute-input-code
            execute-input-counter
            json->execute-input
            execute-input->json

            execute-request?
            execute-request
            execute-request-code
            execute-request-silent?
            execute-request-store-history?
            execute-request-allow-stdin?
            execute-request-stop-on-error?
            json->execute-request
            execute-request->json

            execute-reply?
            execute-reply
            execute-reply-status
            execute-reply-counter
            execute-reply-exception-name
            execute-reply-exception-value
            execute-reply-traceback
            json->execute-reply
            execute-reply->json

            execute-result?
            execute-result
            execute-result-counter
            execute-result-data
            execute-result-metadata
            json->execute-result
            execute-result->json

            stream?
            stream
            stream-name
            stream-text
            json->stream
            stream->json

            inspect-request?
            inspect-request
            inspect-request-code
            inspect-request-cursor-position
            inspect-request-detail-level
            json->inspect-request
            inspect-request->json

            inspect-reply?
            inspect-reply
            inspect-reply-status
            inspect-reply-found?
            inspect-reply-data
            inspect-reply-metadata
            json->inspect-reply
            inspect-reply->json

            complete-request?
            complete-request
            complete-request-code
            complete-request-cursor-position
            json->complete-request
            complete-request->json

            complete-reply?
            complete-reply
            complete-reply-status
            complete-reply-matches
            complete-reply-cursor-start
            complete-reply-cursor-end
            complete-reply-metadata
            json->complete-reply
            complete-reply->json))

;;; Commentary:
;;;
;;; This file implements Jupyter messaging as defined at
;;; <https://jupyter-client.readthedocs.io/en/stable/messaging.html>.
;;;
;;; Code:

;; FIXME: Here we use 'zmq_recv' under the hood, which *truncates* messages
;; when they don't fit in the provided array.  Thus, tell simple-zmq to use a
;; big buffer, in the hope that it'll fit.  64K should be enough for everyone.
;; TODO: Use the 'zmq_msg_t' API instead.
(zmq-set-buffer-size 65536)

;; Jupyter message header as defined at
;; <https://jupyter-client.readthedocs.io/en/stable/messaging.html#general-message-format>.
(define-record-type <header>
  (%header id user session date type version sender)
  header?
  (id           header-id)                         ;bytevector (UUID)
  (user         header-user)                       ;string
  (session      header-session)                    ;bytevector (UUID)
  (date         header-date)                       ;string
  (type         header-message-type)               ;string
  (version      header-version)                    ;string

  ;; This is the ZeroMQ identity of the sender and not part of the header
  ;; itself.
  (sender       header-sender))                    ;bytevector (UUID) | #f


(define %protocol-version "5.0")

(define (current-date-string)
  (date->string (time-utc->date (current-time time-utc))
                "~5.~N"))

(define* (header type user session
                 #:key
                 (id (make-id))
                 (date (current-date-string))
                 (version %protocol-version)
                 sender)
  "Return a new Jupyter message header."
  (%header id user session date type version sender))

(define make-header header)

;; Jupyter message.
(define-immutable-record-type <message>
  (%message header parent-header metadata content buffers)
  message?
  (header          message-header)                ;<header>
  (parent-header   message-parent-header)         ;<header> | #f
  (metadata        message-metadata)              ;hash table (?)
  (content         message-content set-message-content) ;string
  (buffers         message-buffers))              ;bytevector

(define* (message header content
                  #:key parent-header metadata (buffers '()))
  "Return a new Jupyter message."
  (%message header parent-header metadata content buffers))

(define make-message message)

(define (make-id)
  (number->string (random (expt 2 128)) 16))

(define (get-signature key str)
  "Return a hexadecimal string containing the SHA256 HMAC of STR, a string,
with KEY, another string."
  (bytevector->base16-string
   (sign-data key (string->utf8 str)
              #:algorithm (mac-algorithm hmac-sha256))))

(define* (message-parts message key
                        #:key
                        (recipient (and=> (message-parent-header message)
                                          header-sender)))
  "Return the list of parts (bytevectors) of MESSAGE for RECIPIENT, a ZeroMQ
identity (a bytevector) of #f if there's no specific recipient.

This is a low-level procedure for internal use."
  (let* ((header    (header->string (message-header message)))
         (parent    (match (message-parent-header message)
                      (#f "{}")
                      (header (header->string header))))
         (metadata  (or (message-metadata message) "{}"))
         (content   (message-content message))
         (payload   (string-append header parent metadata content))
         (signature (get-signature key payload)))
    ;; Per
    ;; <https://jupyter-client.readthedocs.io/en/latest/messaging.html#the-wire-protocol>,
    ;; the identities part before DELIM "can be zero or more socket
    ;; identities".  Here we accept zero or one.
    `(,@(if recipient (list recipient) '())
      ,DELIM
      ,@(map string->utf8
             (list signature
                   header parent
                   metadata content)))))

(define (reply message type content)
  "Return a Jupyter message that is a reply to MESSAGE."
  (let ((parent (message-header message)))
    (make-message (header type (header-user parent)
                          (header-session parent))
                  content
                  #:parent-header parent)))

(define (message-type message)
  "Return the type of MESSAGE as a string--e.g., \"kernel_info_request\"."
  (header-message-type (message-header message)))

(define (message-sender message)
  "Return the identity (a bytevector) of the sender of MESSAGE or #f."
  (header-sender (message-header message)))

(define* (string->header str #:optional sender)
  "Read the JSON dictionary in STR and return the corresponding <header>
record.  Return #f if STR is the empty dictionary."
  (match (json-string->scm str)
    (()
     #f)
    ((alist ...)
     (header (assoc-ref alist "msg_type")
             (assoc-ref alist "username")
             (assoc-ref alist "session")
             #:id (assoc-ref alist "msg_id")
             #:date (assoc-ref alist "date")
             #:version (assoc-ref alist "version")
             #:sender sender))))

(define (header->string header)
  "Return HEADER as a JSON string."
  (scm->json-string `(("msg_id" . ,(header-id header))
                      ("username" . ,(header-user header))
                      ("session" . ,(header-session header))
                      ,@(match (header-date header)
                          (#f   '())
                          (date `(("date" . ,date))))
                      ("msg_type" . ,(header-message-type header))
                      ("version" . ,(header-version header)))))

(define DELIM (string->utf8 "<IDS|MSG>"))

(define (parts->message parts)
  "Return a message record from PARTS, a list of bytevectors as returned by
'zmq-get-msg-parts-bytevector'.

This is a low-level procedure for internal use."

  (define (delimiter? bv)
    (bytevector=? DELIM bv))

  ;; Note: The "routing prefix", which comes before <IDS|MSG>, "can be zero
  ;; or more socket identities", quoth
  ;; <https://jupyter-client.readthedocs.io/en/stable/messaging.html#messages-on-the-shell-router-dealer-channel>.
  ;; Here we only remember the first one, which is the real sender identity;
  ;; subsequent identities can appear when the message has been relayed.
  (match parts
    ((routing ... (? delimiter?) signature
              (= utf8->string header)
              (= utf8->string parent-header)
              (= utf8->string metadata)
              (= utf8->string content))
     (message (string->header header
                              (match routing
                                (() #f)
                                ((sender _ ...) sender)))
              content
              #:parent-header (string->header parent-header)
              #:metadata metadata))))


;;;
;;; Message types.
;;;

(define %current-protocol-version
  ;; Version of the Jupyter protocol implemented here.
  "5.0.3")

;; Kernel info:
;; <https://jupyter-client.readthedocs.io/en/latest/messaging.html#kernel-info>.
(define-json-mapping <language-info> language-info make-language-info
  language-info?
  json->language-info <=> language-info->json
  (name            language-info-name)
  (version         language-info-version)
  (mime-type       language-info-mime-type
                   (json "mimetype"))
  (file-extension  language-info-file-extension
                   (json "file_extension"))
  (pygments-lexer  language-info-pygments-lexer
                   (json "pygments_lexer"))
  (codemirror-mode language-info-codemirror-mode
                   (json "codemirror_mode")))

(define-json-mapping <kernel-info-reply> kernel-info-reply
  make-kernel-info-reply
  kernel-info-reply?
  json->kernel-info-reply <=> kernel-info-reply->json
  (status            kernel-info-reply-status
                     (default "ok"))
  (protocol-version  kernel-info-reply-protocol-version
                     (json "protocol_version")
                     (default %current-protocol-version))
  (implementation    kernel-info-reply-implementation
                     (default "guile"))
  (implementation-version kernel-info-reply-implementation-version
                          (json "implementation_version")
                          (default (version)))
  (language-info     kernel-info-reply-language-info
                     (json "language_info"
                           json->language-info
                           language-info->json))
  (banner            kernel-info-reply-banner
                     (default ""))
  (help-links        kernel-info-reply-help-links
                     (json "help_links" identity reverse) ;XXX: !
                     (default '())))

(define-json-mapping <kernel-status> kernel-status
  make-kernel-status
  kernel-status?
  json->kernel-status <=> kernel-status->json
  (execution-state  kernel-status-execution-state
                    (json "execution_state" string->symbol symbol->string)
                    (default 'idle)))

(define-json-mapping <execute-input> execute-input
  make-execute-input
  execute-input?
  json->execute-input <=> execute-input->json
  (code             execute-input-code)
  (counter          execute-input-counter
                    (json "execution_count") (default 0)))

(define-json-mapping <execute-request> execute-request
  make-execute-request
  execute-request?
  json->execute-request <=> execute-request->json
  (code             execute-request-code)
  (silent?          execute-request-silent?
                    (json "silent") (default #f))
  (store-history?   execute-request-store-history?
                    (json "store_history") (default #t))
  ;; TODO: Add "user_expressions" dictionary.
  (allow-stdin?     execute-request-allow-stdin?
                    (json "allow_stdin") (default #t))
  (stop-on-error?   execute-request-stop-on-error?
                    (json "stop_on_error") (default #f)))

(define-json-mapping <execute-reply> execute-reply
  make-execute-reply
  execute-reply?
  json->execute-reply <=> execute-reply->json
  (status           execute-reply-status
                    (json "status" string->symbol symbol->string)
                    (default 'ok))
  (counter          execute-reply-counter
                    (json "execution_count"))
  ;; TODO: Add 'payload' and 'user_expressions'.

  ;; The following fields are for status = 'error.
  (exception-name   execute-reply-exception-name
                    (json "ename") (default #f))
  (exception-value  execute-reply-exception-value
                    (json "evalue") (default #f))
  (traceback        execute-reply-traceback
                    (json "traceback" vector->list list->vector)
                    (default '())))

(define-json-mapping <execute-result> execute-result
  make-execute-result
  execute-result?
  json->execute-result <=> execute-result->json
  (counter         execute-result-counter
                   (json "execution_count"))
  (data            execute-result-data (default '()))      ;alist
  (metadata        execute-result-metadata (default '()))) ;alist

(define-json-mapping <stream> stream
  make-stream
  stream?
  json->stream <=> stream->json
  (name            stream-name
                   (json "name" string->symbol symbol->string))
  (text            stream-text))

(define-json-mapping <inspect-request> inspect-request
  make-inspect-request
  inspect-request?
  json->inspect-request <=> inspect-request->json
  (code            inspect-request-code)
  (cursor-position inspect-request-cursor-position
                   (json "cursor_pos"))
  (detail-level    inspect-request-detail-level
                   (json "detail_level") (default 0)))

(define-json-mapping <inspect-reply> inspect-reply
  make-inspect-reply
  inspect-reply?
  json->inspect-reply <=> inspect-reply->json
  (status          inspect-reply-status
                   (json "status" string->symbol symbol->string)
                   (default 'ok))
  (found?          inspect-reply-found? (json "found"))
  (data            inspect-reply-data
                   (json "data") (default '()))
  (metadata        inspect-reply-metadata
                   (json "metadata") (default '())))

(define-json-mapping <complete-request> complete-request
  make-complete-request
  complete-request?
  json->complete-request <=> complete-request->json
  (code            complete-request-code)
  (cursor-position complete-request-cursor-position
                   (json "cursor_pos")))

(define-json-mapping <complete-reply> complete-reply
  make-complete-reply
  complete-reply?
  json->complete-reply <=> complete-reply->json
  (status          complete-reply-status
                   (json "status" string->symbol symbol->string)
                   (default 'ok))
  (matches         complete-reply-matches
                   (json "matches" vector->list list->vector))
  (cursor-start    complete-reply-cursor-start (json "cursor_start"))
  (cursor-end      complete-reply-cursor-end (json "cursor_end"))
  (metadata        complete-reply-metadata
                   (json "metadata") (default '())))
