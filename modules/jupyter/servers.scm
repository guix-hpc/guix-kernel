;;; Guix-kernel -- Guix kernel for Jupyter
;;; Copyright (C) 2018 Pierre-Antoine Rouby <pierre-antoine.rouby@inria.fr>
;;; Copyright (C) 2018, 2019, 2021 Inria
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (jupyter servers)
  #:use-module (jupyter kernels)
  #:use-module (jupyter messages)
  #:use-module (simple-zmq)
  #:use-module (json parser)
  #:use-module (rnrs bytevectors)
  #:use-module (ice-9 match)
  #:use-module (ice-9 vlist)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)
  #:use-module (srfi srfi-71)
  #:export (connection->kernel
            connection-file->kernel

            serve-kernels
            monitor-client
            unmonitor-client
            leave-server-loop))

;;; Commentary:
;;;
;;; This module provides support for implementing Jupyter kernels, as
;;; described at
;;; <https://jupyter-client.readthedocs.io/en/latest/kernels.html>.
;;;
;;; Code:

(define* (connection->kernel connection
                             #:key
                             (name "guix")
                             (context (zmq-create-context)))
  "Return a <kernel> for a server connected to CONNECTION."
  (define (port->uri port)
    (string-append (connection-transport connection) "://"
                   (connection-ip connection) ":"
                   (number->string port)))

  (define (make-socket type port)
    (let* ((uri     (port->uri port))
           (socket  (zmq-create-socket context type)))
      (zmq-bind-socket socket uri)
      socket))

  (kernel name (getpid)
          #:key (string->utf8 (connection-key connection))
          #:control
          (make-socket ZMQ_ROUTER (connection-control-port connection))
          #:shell
          (make-socket ZMQ_ROUTER (connection-shell-port connection))
          #:standard-input
          (make-socket ZMQ_ROUTER (connection-stdin-port connection))
          #:heartbeat
          (make-socket ZMQ_REP (connection-heartbeat-port connection))
          #:iosub
          (make-socket ZMQ_PUB (connection-iopub-port connection))))

(define connection-file->kernel
  ;; Read the contents of a Jupyter \"connection file\" from PORT and return a
  ;; <kernel> record.
  ;; See <https://jupyter-client.readthedocs.io/en/stable/kernels.html#connection-files>.
  (compose connection->kernel json->connection json->scm))


;;;
;;; Server loop.
;;;

;; We would happily use Fibers instead of rolling our own half-baked
;; scheduler.  Alas, zmq uses its own I/O threads by default; it's possible
;; to set it to 0 threads with ZMQ_IO_THREADS but that voids your warranty
;; (and it doesn't work, too.)


(define %server-scheduler-prompt
  ;; Prompt to abort to to communicate with the server loop.
  (make-prompt-tag "kernel-scheduler"))

(define %server-exit-prompt
  ;; Prompt to abort to to exit the server loop.
  (make-prompt-tag "kernel-exit"))

(define (monitor-client kernel)
  "Add KERNEL to the list of clients monitored by the server loop.

This procedure must be called from the dynamic extent of a 'serve-kernels'
call."
  (abort-to-prompt %server-scheduler-prompt 'monitor-kernel kernel))

(define (unmonitor-client kernel)
  "Remove KERNEL from the list of clients monitored by the server loop.

This procedure must be called from the dynamic extent of a 'serve-kernels'
call."
  (abort-to-prompt %server-scheduler-prompt 'unmonitor-kernel kernel))

(define (leave-server-loop state)
  "Leave the current server loop and have it return STATE.

This procedure must be called from the dynamic extent of a 'serve-kernels'
call."
  (abort-to-prompt %server-exit-prompt state))

(define* (serve-kernels kernels handle-message seed
                        #:key (log-port (current-error-port)))
  "Wait for messages sent to KERNELS, a list of <kernel> records as returned
by 'connection->kernel', and pass each of them to HANDLE-MESSAGE.  SEED is
the initial state of the server; it is passed to HANDLE-MESSAGE along with
each message received."
  (define timeout -1)                             ;wait forever

  (define (kernels->socket-lookup kernels)
    ;; Return a procedure that, given a zmq socket, returns the <kernel> among
    ;; KERNELS it belongs to.
    (define table
      (fold (lambda (kernel table)
              (fold (lambda (socket table)
                      (vhash-consq socket kernel table))
                    table
                    (kernel-sockets kernel)))
            vlist-null
            kernels))

    (lambda (socket)
      (match (vhash-assq socket table)
        ((_ . kernel) kernel)
        (#f           #f))))

  (call-with-prompt %server-exit-prompt
    (lambda ()
      (let serve ((kernels        kernels)
                  (socket->kernel (kernels->socket-lookup kernels))
                  (state          seed))
        (define items
          (zmq-poll* (map (lambda (socket)
                            (poll-item socket (logior ZMQ_POLLIN ZMQ_POLLERR)))
                          (append-map kernel-sockets kernels))
                     timeout))

        (define (handle-abort k . args)
          ;; Handle aborts to the prompt.  We need an "+F+" kind of operator
          ;; (info "(guile) Shift and Reset") where we reinstate a prompt
          ;; before invoking the continuation, hence the self-reference.
          (call-with-prompt %server-scheduler-prompt
            (lambda ()
              (let ((state requests (k)))
                ;; Memorize the request ARGS so we can process it later.
                (values state (cons args requests))))
            handle-abort))

        (define (handle-requests requests state)
          ;; Handle the 'monitor-kernel' and 'unmonitor-kernel' REQUESTS.
          (let ((kernels (fold (lambda (request kernels)
                                 (match request
                                   (('monitor-kernel kernel)
                                    (cons kernel kernels))
                                   (('unmonitor-kernel kernel)
                                    (delq kernel kernels))))
                               kernels
                               requests)))
            (format log-port "[~a] now monitoring ~a kernels~%"
                    (getpid) (length kernels))
            (serve kernels
                   (kernels->socket-lookup kernels)
                   state)))

        (let loop ((items items)
                   (state state))
          (match items
            (()
             (serve kernels socket->kernel state))
            ((item rest ...)
             (let* ((socket (poll-item-socket item))
                    (kernel (socket->kernel socket))
                    (kind   (kernel-socket-kind kernel socket))
                    (parts  (zmq-get-msg-parts-bytevector socket)))
               ;; Heartbeat messages are raw "bytestrings" that should be echoed
               ;; back right away.
               (if (eq? kind kernel-heartbeat)
                   (begin
                     (zmq-send-msg-parts-bytevector (kernel-heartbeat kernel)
                                                    parts)
                     (loop rest state))
                   (let* ((message (parts->message parts))
                          (state requests
                                 (call-with-prompt %server-scheduler-prompt
                                   (lambda ()
                                     (values (handle-message kernel kind
                                                             message state)
                                             '()))
                                   handle-abort)))
                     (match requests
                       (()
                        (loop rest state))
                       (_
                        ;; We got a series of requests as aborts to
                        ;; %SERVER-SCHEDULER-PROMPT while calling
                        ;; HANDLE-MESSAGE; process them now.
                        (handle-requests requests state)))))))))))
    (lambda (_ state)
      state)))
