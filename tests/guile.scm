;;; Guix-kernel -- Guix kernel for Jupyter
;;; Copyright (C) 2019, 2021, 2024 Inria
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (tests guile)
  #:use-module (jupyter kernels)
  #:use-module (jupyter messages)
  #:use-module (jupyter servers)
  #:use-module (jupyter guile)
  #:use-module (simple-zmq)
  #:use-module (json parser)
  #:use-module (json builder)
  #:use-module (rnrs bytevectors)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)
  #:use-module (srfi srfi-64)
  #:use-module (srfi srfi-71)
  #:use-module (ice-9 threads)
  #:use-module (ice-9 match))

(define %client-context
  (zmq-create-context))

(define %kernel-key "secretkey")

(define (type-predicate type)
  (lambda (message)
    (string=? (message-type message) type)))


(test-begin "guile")

(test-equal "kernel_info_request"
  (@@ (jupyter guile) %kernel-info-reply)
  (let ((connection client (allocate-connection %client-context
                                                "tcp" "127.0.0.1"
                                                %kernel-key)))
    (define (client-thunk)
      (let ((request (message (header "kernel_info_request" "luser" "12345")
                              "{}")))
        (send-message client request)
        (let ((reply (read-message client 10000)))
          (send-message client
                        (message (header "shutdown_request" "luser" "12345")
                                 "{}"))
          (close-kernel client)
          (json->kernel-info-reply (message-content reply)))))

    (let ((thread (call-with-new-thread client-thunk)))
      (run-guile-kernel connection)
      (join-thread thread))))

(test-equal "execute_request"
  (list (kernel-status (execution-state 'busy))
        (kernel-status (execution-state 'idle))
        (execute-input (code "(begin (* 7 6))"))
        (execute-result (data `(("text/plain" . "42")))
                        (counter 0))
        (execute-reply (status 'ok) (counter 0)))
  (let ((connection client (allocate-connection %client-context
                                                "tcp" "127.0.0.1"
                                                %kernel-key)))
    (define (client-thunk)
      (let ((request (message (header "execute_request" "luser" "12345")
                              (scm->json-string
                               (execute-request->json
                                (execute-request (code "(* 7 6)")))))))
        (send-message client request)
        (let ((replies (unfold (cut > <> 4)
                               (lambda (_)
                                 (read-message client 10000))
                               1+ 0)))
          (send-message client
                        (message (header "shutdown_request" "luser" "12345")
                                 "{}"))
          (close-kernel client)
          (map (lambda (message)
                 (let ((content (message-content message)))
                   (match (message-type message)
                     ("status" (json->kernel-status content))
                     ("execute_input" (json->execute-input content))
                     ("execute_result" (json->execute-result content))
                     ("execute_reply" (json->execute-reply content)))))
               (append (filter (type-predicate "status") replies)
                       (map (lambda (type)
                              (find (type-predicate type) replies))
                            '("execute_input" "execute_result"
                              "execute_reply")))))))

    (let ((thread (call-with-new-thread client-thunk)))
      (run-guile-kernel connection)
      (join-thread thread))))

(test-equal "execute_request, exception"
  "system-error"
  (let ((connection client (allocate-connection %client-context
                                                "tcp" "127.0.0.1"
                                                %kernel-key)))
    (define (client-thunk)
      (let ((request (message (header "execute_request" "luser" "12345")
                              (scm->json-string
                               (execute-request->json
                                (execute-request (code "(delete-file \"/\")")))))))
        (send-message client request)
        (let ((replies (unfold (cut > <> 4)
                               (lambda (_)
                                 (read-message client 10000))
                               1+ 0)))
          (send-message client
                        (message (header "shutdown_request" "luser" "12345")
                                 "{}"))
          (close-kernel client)
          (json->execute-reply
           (message-content (find (type-predicate "execute_reply")
                                  replies))))))

    (let ((thread (call-with-new-thread client-thunk)))
      (run-guile-kernel connection)
      (let ((reply (join-thread thread)))
        (and (pair? (execute-reply-traceback reply))
             (execute-reply-exception-name reply))))))

(test-equal "execute_request, stdout/stderr"
  (list (stream (name 'stdout) (text "hey!\n"))
        (stream (name 'stderr) (text "ho!\n")))
  (let ((connection client (allocate-connection %client-context
                                                "tcp" "127.0.0.1"
                                                %kernel-key)))
    (define (client-thunk)
      (let ((request (message (header "execute_request" "luser" "12345")
                              (scm->json-string
                               (execute-request->json
                                (execute-request
                                 (code
                                  (object->string
                                   '(begin
                                      (display "hey!\n"
                                               (current-output-port))
                                      (usleep 500000)
                                      (display "ho!\n"
                                               (current-error-port)))))))))))
        (send-message client request)
        (let ((replies (unfold (cut > <> 4)
                               (lambda (_)
                                 (read-message client 10000))
                               1+ 0)))
          (send-message client
                        (message (header "shutdown_request" "luser" "12345")
                                 "{}"))
          (close-kernel client)
          (filter (type-predicate "stream") replies))))

    (let ((thread (call-with-new-thread client-thunk)))
      (run-guile-kernel connection)
      (map (compose json->stream message-content)
           (join-thread thread)))))

(test-equal "complete_request"
  (complete-reply (status 'ok)
                  (matches '("l:alist-cons" "l:alist-copy"
                             "l:alist-delete" "l:alist-delete!"))
                  (cursor-start 1)                ;XXX: what does it mean?
                  (cursor-end 9))
  (let ((connection client (allocate-connection %client-context
                                                "tcp" "127.0.0.1"
                                                %kernel-key)))
    (define (client-thunk)
      (send-message client
                    (message (header "execute_request" "luser" "12345")
                             (scm->json-string
                              (execute-request->json
                               (execute-request
                                (code
                                 (object->string
                                  '(use-modules ((srfi srfi-1)
                                                 #:prefix l:)))))))))
      (let loop ((i 10))
        (if (zero? i)
            (error "did not receive 'execute_reply'")
            (let ((message (read-message client 10000)))
              (if (string=? (message-type message) "execute_reply")
                  (pk 'execute-reply (message-content message))
                  (loop (- i 1))))))

      (send-message client
                    (message (header "complete_request" "luser" "12345")
                             (scm->json-string
                              (complete-request->json
                               (complete-request
                                (code "(l:alist-")
                                (cursor-position (string-length code)))))))

      (let ((reply (let loop ()
                     (let ((message (read-message client 10000)))
                       (and message
                            (if (string=? (message-type message)
                                          "complete_reply")
                                message
                                (loop)))))))
        (send-message client
                      (message (header "shutdown_request" "luser" "12345")
                               "{}"))
        (close-kernel client)
        reply))

    (let ((thread (call-with-new-thread client-thunk)))
      (run-guile-kernel connection)
      (and=> (join-thread thread)
             (compose json->complete-reply message-content)))))

(test-end "guile")
