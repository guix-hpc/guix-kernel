;;; Guix-kernel -- Guix kernel for Jupyter
;;; Copyright (C) 2018  Pierre-Antoine Rouby <pierre-antoine.rouby@inria.fr>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (tests hmac)
  #:use-module (jupyter messages)
  #:use-module (srfi srfi-64))

(define get-signature
  (@@ (jupyter messages) get-signature))

(test-begin "hmac")

(test-equal "unit: (get-signature empty string)"
  "b613679a0814d9ec772f95d778c35fc5ff1697c493715653c6c712144292c5ad"
  (get-signature "" ""))

(test-equal "unit: (get-signature foo bar)"
  "f9320baf0249169e73850cd6156ded0106e2bb6ad8cab01b7bbbebe6d1065317"
  (get-signature "foo" "bar"))

(test-equal "unit: (get-signature ADSILLH inria)"
  "36ba1d2bb2c5c7d856a5ceb130c4c2231e0c4142a95af5dfeca7c054a101780f"
  (get-signature "ADSILLH" "inria"))

(test-equal "unit: (get-signature Free Software Free Society)"
  "32a9099a7cd02a82d217e792556afc8ac1d4557d4400332b5f2f5ad47413f05c"
  (get-signature "Free-Software" "Free-Society"))

;; Example from <https://en.wikipedia.org/wiki/HMAC>.
(test-equal "unit: (get-signature wikipedia example)"
  "f7bc83f430538424b13298e6aa6fb143ef4d59a14946175997479dbc2d1a3cd8"
  (get-signature "key" "The quick brown fox jumps over the lazy dog"))

(test-end "hmac")
