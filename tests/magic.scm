;;; Guix-kernel -- Guix kernel for Jupyter
;;; Copyright (C) 2018  Pierre-Antoine Rouby <pierre-antoine.rouby@inria.fr>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (tests magic)
  #:use-module (guix jupyter magic)
  #:use-module (srfi srfi-64))

(define magic-no-code   ";;guix environment test-magic <- jupyter-guile-kernel guile@2.2.3")
(define no-magic-code   "(define hello \"hello world\")")
(define no-magic        ";; this is not magic command")
(define no-magic-2      ";; guix is not magic command")
(define magic-code      (string-append magic-no-code "\n" no-magic-code))
(define no-magic-code-2 (string-append no-magic      "\n" no-magic-code))

(define magic-env    magic-no-code)

(test-begin "magic")

;;
;; Magic?
;;

(test-equal "unit: (magic? magic-no-code)"
  #t (magic? magic-no-code))

(test-equal "unit: (magic? magic-code)"
  #t (magic? magic-code))

(test-equal "unit: (magic? no-magic)"
  #f (magic? no-magic))

(test-equal "unit: (magic? no-magic-2)"
  #f (magic? no-magic-2))

;;
;; Get magic line.
;;

(test-equal "unit: (get-magic-line magic-no-code)"
  magic-no-code
  (get-magic-line magic-no-code))

(test-equal "unit: (get-magic-line magic-code)"
  magic-no-code
  (get-magic-line magic-code))

(test-equal "unit: (get-magic-line no-magic)"
  #f
  (get-magic-line no-magic))

(test-equal "unit: (get-magic-line no-magic-code)"
  #f
  (get-magic-line no-magic-code))

;;
;; Magic attr?
;;

(test-equal "unit: (magic-attr? magic-code \"environment\")"
  #t (magic-attr? magic-code "environment"))

(test-equal "unit: (magic-env? magic-env)"
  #t (magic-env? magic-env))

(test-equal "unit: (magic-env no-magic)"
  #f (magic-env? no-magic))

(test-end "magic")
